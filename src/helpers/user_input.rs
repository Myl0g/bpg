use std::io::{stdin, stdout, Write};

pub fn get_user_input(prompt: &str) -> String {
    // https://users.rust-lang.org/t/how-to-get-user-input/5176/3
    let mut s = String::new();
    print!("{}: ", prompt);
    let _ = stdout().flush();
    stdin()
        .read_line(&mut s)
        .expect("Did not enter a correct string");
    if let Some('\n') = s.chars().next_back() {
        s.pop();
    }
    if let Some('\r') = s.chars().next_back() {
        s.pop();
    }

    return String::from(s);
}

pub fn get_pgp_msg_input() -> String {
    let mut s = String::new();
    println!("Paste your encrypted PGP message in below.");
    let _ = stdout().flush();

    loop {
        let mut tmp = String::new();
        stdin()
            .read_line(&mut tmp)
            .expect("Did not enter a correct string");
        if let Some('\n') = s.chars().next_back() {
            tmp.pop();
        }
        if let Some('\r') = s.chars().next_back() {
            tmp.pop();
        }

        s += "\n";
        s += &tmp;
        if &tmp == "-----END PGP MESSAGE-----" {
            break;
        }
    }

    return String::from(s);
}
