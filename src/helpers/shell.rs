use std::io::{self, Write};
use std::process::Command;

pub fn cmd(to_run: &str, args: &str) {
    let output = Command::new(to_run)
        .args(String::from(args).split_terminator(" "))
        .output()
        .expect("Command failed!");
    io::stdout().write_all(&output.stdout).unwrap();
    io::stderr().write_all(&output.stderr).unwrap();
}
