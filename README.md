# Better Privacy Guard

Wraps around `gpg` to provide a nicer UI.

```
bpg 2.2.0
Milo Gilad <myl0gcontact@gmail.com>
Simple wrapper around gpg

USAGE:
    bpg [SUBCOMMAND]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    decrypt     Decrypts a message
    encrypt     Encrypts a message
    help        Prints this message or the help of the given subcommand(s)
    keys        Manages keys in your gpg keychain
    tutorial    Basic overview of GPG/BPG and how public/private key crypto works.
```

# Installation

## Automatic

Using macOS? Have [Homebrew](https://brew.sh) installed?

```bash
brew tap Myl0g/homebrew-myl0g https://gitlab.com/myl0g/homebrew-myl0g.git
brew install bpg

# if the brew install command does not work as intended, use this:
brew install myl0g/homebrew-myl0g/bpg
```

## Manual

Besides requiring `gpg`, we also require a graphical `pinentry` program to use keys. Here are basic install instructions for macOS:

```bash
brew install gnupg
brew install pinentry-mac
echo "pinentry-program /usr/local/bin/pinentry-mac" >> ~/.gnupg/gpg-agent.conf
killall gpg-agent
```

After that, you can either grab a pre-built binary off of the [tags page](https://gitlab.com/Myl0g/bpg/tags), or you can build from source:

### Build From Source

Given that we're coding this in Rust, install it from [here](https://www.rust-lang.org/learn/get-started). Then, follow these steps (UNIX-like systems):

```bash
cd bpg
cargo build --release # target/release/bpg is the binary's location
```

# TODO

* improved key editing
* auto-updating
